# coding: utf8

from __future__ import unicode_literals
import ckan.authz as authz
from ckan.common import _
from ckan.logic import get_or_bust
import ckan.logic.auth as logic_auth
from ckan.lib.plugins import get_permission_labels
import ckan.plugins.toolkit as toolkit
from ckanext.restricted import logic
from ckan.logic.action.get import package_show

from logging import getLogger

log = getLogger(__name__)


@toolkit.auth_allow_anonymous_access
def restricted_resource_show(context, data_dict=None):

    # Ensure user who can edit the package can see the resource
    resource = data_dict.get("resource", context.get("resource", {}))
    if not resource:
        resource = logic_auth.get_resource_object(context, data_dict)
    if type(resource) is not dict:
        resource = resource.as_dict()

    if authz.is_authorized(
        "package_update", context, {"id": resource.get("package_id")}
    ).get("success"):
        return {"success": True}

    user_name = logic.restricted_get_username_from_context(context)

    package = data_dict.get("package", {})
    if not package:
        model = context["model"]
        package = model.Package.get(resource.get("package_id"))
        package = package.as_dict()
    return logic.restricted_check_user_resource_access(user_name, resource, package)


@toolkit.auth_allow_anonymous_access
def restricted_package_show(context, data_dict=None):
    user = context.get("user")
    package = logic_auth.get_package_object(context, data_dict)

    labels = get_permission_labels()
    user_labels = labels.get_user_dataset_labels(context["auth_user_obj"])
    authorized = any(dl in user_labels for dl in labels.get_dataset_labels(package))
    authorized = authorized and (
        context.get("auth_user_obj") is not None
        or package.get("logged_in_only") == "false"
    )
    if not authorized:
        return {
            "success": False,
            "msg": _("User %s not authorized to read package %s") % (user, package.id),
        }
    else:
        return {"success": True}
